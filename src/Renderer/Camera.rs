use math::matrix::Matrix4;
use math::vector::Vector3;

#[derive(Copy, Clone)]
pub struct Camera
{
	target : Vector3<f32>,
	x_angle : f32,
	y_angle : f32,
	radius : f32,

	z_near : f32,
	z_far : f32,
	aspect_ratio : f32,
	fov : f32,
	changed : bool
}

impl Camera
{
	pub fn new(x: f32, y : f32, target : Vector3<f32>, rad : f32, fov : f32, z_near: f32, z_far: f32, ratio: f32) -> Camera
	{
		Camera{target : target, x_angle : x, y_angle : y, radius : rad, z_near: z_near, z_far : z_far, aspect_ratio : ratio, fov : fov, changed : true}
	}

	pub fn view_matrix(target: Vector3<f32>, xang : f32, yang: f32, rad : f32) -> Matrix4<f32>
	{
		Matrix4::translation(0.0, 0.0, rad) * Matrix4::rotation_x(xang/180.0 * 3.14) * Matrix4::rotation_y(yang/180.0 * 3.14) * Matrix4::translation(target.x(), target.y(), target.z())
	}

	fn projection_matrix(fov : f32, z_near : f32, z_far : f32, aspect: f32) -> Matrix4<f32>
	{
		Matrix4::frustum(fov, aspect, z_near, z_far)
	}

	pub fn get_view_matrix(&self) -> Matrix4<f32>
	{
		Camera::view_matrix(self.target, self.x_angle, self.y_angle, self.radius)
	}

	pub fn get_projection_matrix(&self) -> Matrix4<f32>
	{
		Camera::projection_matrix(self.fov, self.z_near, self.z_far, self.aspect_ratio)
	}

	pub fn set_y_angle(&mut self, angle : f32)
	{
		self.y_angle = angle;
		self.changed = true;
	}

	pub fn modify_y_angle(&mut self, d_y : f32)
	{
		let ang = self.y_angle + d_y;
		self.set_y_angle(ang);
	}

	pub fn set_x_angle(&mut self, angle : f32)
	{
		self.x_angle = angle;
		self.changed = true;
		if self.x_angle > 90.0
		{
			self.x_angle = 90.0;
		}
		if self.x_angle < -90.0
		{
			self.x_angle = -90.0;
		}
	}

	pub fn modify_x_angle(&mut self, d_x : f32)
	{
		let ang = self.x_angle + d_x;
		self.set_x_angle(ang);
	}

	pub fn set_radius(&mut self, radius : f32)
	{
		self.radius = radius;
		self.changed = true;
				if self.radius < 0.1
		{
			self.radius = 0.1;
		}
	}

	pub fn modify_radius(&mut self, d_r : f32)
	{
		let rad = self.radius + d_r;
		self.set_radius(rad);
	}

	pub fn get_position(&self) -> Vector3<f32>
	{
		let yang = (self.y_angle - 90.0)/180.0 * 3.14;
		let xang = (-self.x_angle - 90.0)/180.0 * 3.14;
		let x = yang.cos() * (xang).sin();
		let z = yang.sin() * (xang).sin();
		let y = (xang).cos();

		self.target + Vector3::new(-x,y,-z) * self.radius
	}
	
}