pub mod keyboard_input;
pub mod mouse_input;

extern crate glium;
use glium::glutin::*;
use self::keyboard_input::KeyCode;

pub struct InputState {
    pub keyboard: keyboard_input::KeyboardState,
    pub mouse: mouse_input::MouseState
}

impl Default for InputState
{
	fn default() -> InputState
	{
		InputState
		{
			keyboard: Default::default(),
			mouse: Default::default()
		}
	}
}

impl InputState {
    pub fn keyboard_input(&mut self, element_state: ElementState, key: Option<VirtualKeyCode>)
    {
        let val: bool = match element_state {
            glium::glutin::ElementState::Pressed => true,
            glium::glutin::ElementState::Released => false,
        };
        match key {
            Some(key_code) => {
                self.keyboard.set_button(KeyCode::from_glutin_enum(key_code), val)
            }
            _ => (),
        }
    }
}