use window::engine::App;
use window;
use window::engine::Facade as Facade;
use input::InputState;
use input::keyboard_input::KeyboardState;
use input::mouse_input::MouseState;
use glium::Frame;
use engine::rgine_configs::RgineConfig;
use serde_json;

pub struct RgineApp {
    window_name: String,
    configuration: Option<RgineConfig>
}

impl RgineApp {
    pub fn new<S: Into<String>>(init_file_name: S) -> RgineApp {
        // readConfig
        use utils::files::load_file_content;

        let config_file = load_file_content(&init_file_name.into());
        let deserialized: RgineConfig = serde_json::from_str(&config_file).unwrap();
        let name = deserialized.window_name.clone();
        println!("deserialized = {:?}", deserialized);
        RgineApp{window_name :name, configuration: Some(deserialized) }
    }
}

impl App for RgineApp {
    fn has_window_configuration(&self) -> bool {true}

    fn configure_window<'a>(&self, conf: &'a mut window::engine::WindowConfiguration)  -> &'a mut window::engine::WindowConfiguration{
        conf.window_name = self.window_name.clone();
        conf
    }

    fn initialize(&mut self, display: &Facade) {

    }

    fn run_logic(&mut self, input: &InputState, should_end: &mut bool) {

    }

    fn run_render(&mut self, display: &mut Frame) {

    }
}