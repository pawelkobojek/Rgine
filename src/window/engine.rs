extern crate glium;

pub use glium::backend::glutin_backend::GlutinFacade as Facade;
use glium::backend::glutin_backend::PollEventsIter;
use input::keyboard_input::*;
use input::mouse_input::*;
use input::InputState;

pub trait App {
    fn has_window_configuration(&self) -> bool { false }
    fn configure_window<'a>(&self, conf: &'a mut WindowConfiguration) -> &'a mut WindowConfiguration { conf }
    fn initialize(&mut self, display: &Facade);
    fn run_logic(&mut self, input: &InputState, should_end: &mut bool);
    fn run_render(&mut self, display: &mut glium::Frame);
}

pub struct WindowConfiguration {
    pub window_name : String,
    pub full_screen: bool,

}

impl WindowConfiguration {
    pub fn new() -> WindowConfiguration {
        WindowConfiguration{ window_name: String::from("SuperWindow"), full_screen: false}
    }
}

pub struct Engine
{
    input: InputState,
}

impl Engine
{
    pub fn new() -> Engine {
        Engine {input: Default::default()}
    }
}


impl Engine
{
    fn create_configured_window(&self, conf: WindowConfiguration) -> Facade {
        use glium::{DisplayBuild};
        
        let display = glium::glutin::WindowBuilder::new()
            .with_title(conf.window_name)
            .build_glium()
            .unwrap();
        display
    }

    fn iterate_event(&mut self, iter: PollEventsIter)-> bool {
        for ev in iter {
            match ev {
                glium::glutin::Event::Closed => return true,
                glium::glutin::Event::KeyboardInput(state, _, elem) => self.input.keyboard_input(state,elem),
                glium::glutin::Event::MouseMoved(x, y) => {
                    self.input.mouse.set_new_position([x as f32, y as f32]);
                }
                glium::glutin::Event::MouseInput(state, elem) => {
                    let val: bool = match state {
                        glium::glutin::ElementState::Pressed => true,
                        glium::glutin::ElementState::Released => false,
                    };
                    match elem {
                        glium::glutin::MouseButton::Left => self.input.mouse.set_button_state(0, val),
                        glium::glutin::MouseButton::Right => self.input.mouse.set_button_state(1, val),
                        _ => (),
                    }
                }
                _ => (),
            }
        }
        return false;
    }

    pub fn run(&mut self, application:& mut App) {
        let mut conf = WindowConfiguration::new();
        if application.has_window_configuration() {
            application.configure_window(&mut conf);
        }
        let display = self.create_configured_window(conf);
        application.initialize(&display);

        let mut should_end : bool = false;
        loop {
            application.run_logic(&self.input, &mut should_end);
            if should_end {
                return;
            }
            let mut target = display.draw();
            application.run_render(&mut target);
            target.finish().unwrap();

		    self.input.mouse.mouse_moved = false;
		    self.input.keyboard.next_frame();
            if self.iterate_event(display.poll_events()) {
                return;
            }
        }
    }
}
